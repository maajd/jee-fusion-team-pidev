package tn.edu.esprit.sigma.fusion.youbayclient.crud;

import static org.junit.Assert.*;


import java.util.List;
import java.util.UUID;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.AssistantItems;
import tn.edu.esprit.sigma.fusion.youbayclient.basicfonctionnalities.delegate.BasicFonctionnalitiesDelegate;
import tn.edu.esprit.sigma.fusion.youbayclient.basicfonctionnalities.delegate.InitDatabaseServicesDelegate;

public class BasicFonctionnalitiesDelegateTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		InitDatabaseServicesDelegate.doTruncateAllTables();

		/*
		 * We assume that doPopulateDatabase adds at least one instance of each
		 * JPA entity
		 */
		InitDatabaseServicesDelegate.doPopulateDatabase();
	}

	@After
	public void tearDown() throws Exception {
	}

	public String randomString() {
		return UUID.randomUUID().toString();
	}

	@Test
	public void testDoAddAssistantItems() {

		String beacon = randomString();

		AssistantItems newItem = new AssistantItems();

		newItem.setQuestionText(beacon);

		BasicFonctionnalitiesDelegate.doAddAssistantItems(newItem);

		List<AssistantItems> list = BasicFonctionnalitiesDelegate
				.doFindAllAssistantItems();

		assertNotEquals(null, list);
		assertNotEquals(0, list.size());


		for (AssistantItems item : list) {
			if (item.getQuestionText().equals(beacon))
				return; // We found the added item !
		}

		assertNotEquals(true, false); // We did not exit this function, this means the
							// newItem was not found.
		
	}

	@Test
	public void testDoFindAssistantItemsById() {
		List<AssistantItems> list = BasicFonctionnalitiesDelegate
				.doFindAllAssistantItems();
		
		assertNotEquals(null, list);
		assertNotEquals(0, list.size());

		long theId = list.get(0).getAssistantItemsId();

		AssistantItems item = BasicFonctionnalitiesDelegate
				.doFindAssistantItemsById(theId);

		assertNotEquals(item, null);
		
		
	}

	@Test
	public void testDoUpdateAssistantItems() {
		List<AssistantItems> list = BasicFonctionnalitiesDelegate
				.doFindAllAssistantItems();
		
		assertNotEquals(null, list);
		assertNotEquals(0, list.size());
		
		AssistantItems item = list.get(0);

		String beacon = randomString();

		item.setQuestionText(beacon);

		BasicFonctionnalitiesDelegate.doUpdateAssistantItems(item);

		AssistantItems updatedItem = BasicFonctionnalitiesDelegate
				.doFindAssistantItemsById(item.getAssistantItemsId());

		assertEquals(beacon, updatedItem.getQuestionText());
	}

	@Test
	public void testDoDeleteAssistantItems() {
		List<AssistantItems> list = BasicFonctionnalitiesDelegate
				.doFindAllAssistantItems();
		
		assertNotEquals(null,list);
		assertNotEquals(0, list.size());
		
		AssistantItems item = list.get(0);
		Long theId = item.getAssistantItemsId();
		BasicFonctionnalitiesDelegate.doDeleteAssistantItems(item);

		assertEquals(null, BasicFonctionnalitiesDelegate
				.doFindAssistantItemsById(theId));
		
	}

	@Test
	public void testDoDeleteAssistantItemsById() {
		List<AssistantItems> list = BasicFonctionnalitiesDelegate
				.doFindAllAssistantItems();
		
		assertNotEquals(null,list);
		assertNotEquals(0, list.size());
		
		AssistantItems item = list.get(0);
		Long theId = item.getAssistantItemsId();
		BasicFonctionnalitiesDelegate.doDeleteAssistantItemsById(item.getAssistantItemsId());

		assertEquals(null, BasicFonctionnalitiesDelegate
				.doFindAssistantItemsById(theId));
	}

	@Test
	public void testDoFindAllAssistantItems() {
		List<AssistantItems> list = BasicFonctionnalitiesDelegate
				.doFindAllAssistantItems();
		
				assertNotEquals(list,null);
				assertNotEquals(list.size(), 0);
	}

	@Test
	public void testDoAddAuction() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoFindAuctionById() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoUpdateAuction() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoDeleteAuction() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoDeleteAuctionById() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoFindAllAuction() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoAddBuyer() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoFindBuyerById() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoUpdateBuyer() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoDeleteBuyerById() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoFindAllBuyer() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoAddCategory() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoFindCategoryById() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoUpdateCategory() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoDeleteCategory() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoDeleteCategoryById() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoFindAllCategory() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoAddCustomizedAds() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoFindCustomizedAdsById() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoUpdateCustomizedAds() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoDeleteCustomizedAds() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoDeleteCustomizedAdsById() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoFindAllCustomizedAds() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoAddHistoryOfViews() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoFindHistoryOfViewsById() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoUpdateHistoryOfViews() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoDeleteHistoryOfViewsById() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoFindAllHistoryOfViews() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoAddManager() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoFindManagerById() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoUpdateManager() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoDeleteManager() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoDeleteManagerById() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoFindAllManager() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoAddOrderAndReview() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoFindOrderAndReviewById() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoUpdateOrderAndReview() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoDeleteOrderAndReviewById() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoFindAllOrderAndReview() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoAddProductHistory() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoFindProductHistoryById() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoUpdateProductHistory() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoDeleteProductHistory() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoDeleteProductHistoryById() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoFindAllProductHistory() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoAddProduct() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoFindProductById() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoUpdateProduct() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoDeleteProduct() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoDeleteProductById() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoFindAllProduct() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoAddSeller() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoFindSellerById() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoUpdateSeller() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoDeleteSeller() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoDeleteSellerById() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoFindAllSeller() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoFindSpecialPromotionById() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoUpdateSpecialPromotion() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoDeleteSpecialPromotion() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoDeleteSpecialPromotionById() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoFindAllSpecialPromotion() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoAddSubcategory() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoFindSubcategoryById() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoUpdateSubcategory() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoDeleteSubcategory() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoDeleteSubcategoryById() {
		 fail("Not yet implemented");
	}

	@Test
	public void testDoFindAllSubcategory() {
		 fail("Not yet implemented");
	}

}
