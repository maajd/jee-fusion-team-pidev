package tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices;

import java.util.List;

import javax.ejb.Local;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Manager;

@Local
public interface ManagerServicesLocal {

	Boolean addManager(Manager theManager);

	Manager findManagerById(Long theId);

	Boolean updateManager(Manager theManager);

	Boolean deleteManager(Manager theManager);

	Boolean deleteManagerById(Long theId);

	List<Manager> findAllManager();

}
