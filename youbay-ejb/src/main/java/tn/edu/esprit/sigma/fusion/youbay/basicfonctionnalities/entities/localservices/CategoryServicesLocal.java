package tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices;

import java.util.List;

import javax.ejb.Local;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Category;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Subcategory;

@Local
public interface CategoryServicesLocal {

	Boolean addCategory(Category theCategory);

	Category findCategoryById(Long theId);

	Boolean updateCategory(Category theCategory);

	Boolean deleteCategory(Category theCategory);

	Boolean deleteCategoryById(Long theId);

	List<Subcategory> findAllCategory();

}
