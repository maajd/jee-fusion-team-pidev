package tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices;

import java.util.List;

import javax.ejb.Local;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Buyer;

@Local
public interface BuyerServicesLocal {

	Boolean addBuyer(Buyer theBuyer);

	Buyer findBuyerById(Long theId);

	Boolean updateBuyer(Buyer theBuyer);

	Boolean deleteBuyer(Buyer theBuyer);

	Boolean deleteBuyerById(Long theId);

	List<Buyer> findAllBuyer();

}
