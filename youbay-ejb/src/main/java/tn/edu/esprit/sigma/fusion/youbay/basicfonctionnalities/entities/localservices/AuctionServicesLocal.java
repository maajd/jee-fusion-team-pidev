package tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices;

import java.util.List;

import javax.ejb.Local;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Auction;

@Local
public interface AuctionServicesLocal {

	Boolean addAuction(Auction theAuction);

	Auction findAuctionById(Long theId);

	Boolean updateAuction(Auction theAuction);

	Boolean deleteAuction(Auction theAuction);

	Boolean deleteAuctionById(Long theId);

	List<Auction> findAllAuction();

}
