package tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices;

import java.util.List;

import javax.ejb.Remote;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Buyer;

@Remote
public interface BuyerServicesRemote {

	Boolean addBuyer(Buyer theBuyer);

	Buyer findBuyerById(Long theId);

	Boolean updateBuyer(Buyer theBuyer);

	Boolean deleteBuyer(Buyer theBuyer);

	Boolean deleteBuyerById(Long theId);

	List<Buyer> findAllBuyer();

}
