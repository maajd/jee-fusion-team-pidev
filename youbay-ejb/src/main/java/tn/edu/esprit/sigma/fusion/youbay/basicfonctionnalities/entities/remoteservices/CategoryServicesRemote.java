package tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices;

import java.util.List;

import javax.ejb.Remote;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Category;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Subcategory;

@Remote
public interface CategoryServicesRemote {

	Boolean addCategory(Category theCategory);

	Category findCategoryById(Long theId);

	Boolean updateCategory(Category theCategory);

	Boolean deleteCategory(Category theCategory);

	Boolean deleteCategoryById(Long theId);

	List<Subcategory> findAllCategory();

}
