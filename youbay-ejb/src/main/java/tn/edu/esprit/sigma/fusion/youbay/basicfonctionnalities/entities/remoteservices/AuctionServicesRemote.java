package tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices;

import java.util.List;

import javax.ejb.Remote;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Auction;

@Remote
public interface AuctionServicesRemote {

	Boolean addAuction(Auction theAuction);

	Auction findAuctionById(Long theId);

	Boolean updateAuction(Auction theAuction);

	Boolean deleteAuction(Auction theAuction);

	Boolean deleteAuctionById(Long theId);

	List<Auction> findAllAuction();

}
