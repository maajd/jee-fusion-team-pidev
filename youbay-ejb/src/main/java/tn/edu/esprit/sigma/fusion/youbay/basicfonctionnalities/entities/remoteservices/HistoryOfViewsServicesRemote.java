package tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices;

import java.util.List;

import javax.ejb.Remote;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.HistoryOfViews;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.HistoryOfViewsId;

@Remote
public interface HistoryOfViewsServicesRemote {

	Boolean addHistoryOfViews(HistoryOfViews theHistoryOfViews);

	HistoryOfViews findHistoryOfViewsById(
			HistoryOfViewsId theHistoryOfViewsIdClass);

	Boolean updateHistoryOfViews(HistoryOfViews theHistoryOfViews);

	Boolean deleteHistoryOfViewsById(HistoryOfViewsId theHistoryOfViewsIdClass);

	List<HistoryOfViews> findAllHistoryOfViews();

}
